<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>EmbedSocial Filter Filip Simonovikj</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  <script>
    $(function() {
      $("#from").datepicker();
    });

    $(function() {
      $("#to").datepicker();
    });
  </script>
</head>

<body>

  <div class="navbar mb-3">
    <div class="container">
      <h2 class="text-center">Filter</h2>
    </div>
  </div>

  <div class="container">
    <form class="col-8 offset-2 form-horizontal" action="index.php" method="POST">
      <div class="form-group row">
        <label class="col-lg-12 col-md-4 control-label">Order by rating: </label>
        <div class="col-lg-12 col-md-8">
          <select class="form-control" name="order_rating">
            <option>Select</option>
            <option value="highest_first">Highest First</option>
            <option value="lowest_first">Lowest First</option>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-lg-12 col-md-4 control-label">Minimum rating: </label>
        <div class="col-lg-12 col-md-8">
          <select class="form-control" name="minimum_rating">
            <option>Select</option>
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-lg-12 col-md-4 control-label">Prioritize by text: </label>
        <div class="col-lg-12 col-md-8">
          <select name="prioritize_text" class="form-control">
            <option>Select</option>
            <option value="prioritize_text_yes">Yes</option>
            <option value="prioritize_text_no">No</option>
          </select>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-lg-12 col-md-4 control-label">Order by date: </label>
        <div class="col-lg-12 col-md-8">
          <select name="order_by_date" class="form-control">
            <option>Select</option>
            <option value="order_date_newest">Newnest First</option>
            <option value="order_date_oldest">Oldest First</option>
          </select>
        </div>
      </div>


      <div class="form-group row">
        <div class="col-lg-10 col-md-8">
          <input type="submit" name="submit" class="btn btn-primary">
        </div>
      </div>

    </form>
    <div class="row">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>reviewText</th>
            <th>createOn</th>
            <th>reviewName</th>
            <th>rating</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $data = file_get_contents("reviews.json");
          $data = json_decode($data, true);

          // $orderRating =  $_POST['order_rating'];
          // $minimumRating = $_POST['minimum_rating'];
          // $prioritizeText = $_POST['prioritize_text'];
          // $orderByDate = $_POST['order_by_date'];

          if (isset($_POST['submit']) && ($_POST['prioritize_text'] === "prioritize_text_yes")) {
            $prioritizeText = array_column($data, 'reviewFullText');
            array_column($prioritizeText, SORT_DESC);
            sort($data);
            foreach ($data as $row) {
              echo '<tr>
                      <td>' . $row["id"] . '</td>
                      <td>' . $row["reviewFullText"] . '</td>
                      <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                      <td>' . $row["reviewerName"] . '</td>
                      <td>' . $row["rating"] . '</td>
                      </tr>';
            }
          }
          if (isset($_POST['submit']) && ($_POST['prioritize_text'] === "prioritize_text_no")) {
            $prioritizeText = array_column($data, 'reviewFullText');
            array_column($prioritizeText, SORT_NUMERIC, SORT_DESC);
            ksort($data);
            foreach ($data as $row) {
              echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
            }
          }
          if (isset($_POST['submit']) && ($_POST['order_rating'] == 'highest_first')) {
            $highRating = array_column($data, 'rating');
            array_multisort($highRating, SORT_DESC, $data);
            foreach ($data as $row) {
              echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
            }
          }
          if (isset($_POST['submit']) && ($_POST['order_rating'] == 'lowest_first')) {
            $highRating = array_column($data, 'rating');
            array_multisort($highRating, SORT_ASC, $data);
            foreach ($data as $row) {
              echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
            }
          }
          if (isset($_POST['submit']) && (isset($_POST['minimum_rating'])) != "") {
            foreach ($data as $row) {
              if ($_POST['minimum_rating'] <= $row["rating"]) {
                echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
              }
            }
          }
          if (isset($_POST['submit']) && ($_POST['order_by_date'] == "order_date_newest")) {
            $orderNew = array_column($data, 'reviewCreatedOnTime');
            array_multisort($orderNew, SORT_ASC, $data);

            foreach ($data as $row) {
              echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
            }
          }
          if (isset($_POST['submit']) && ($_POST['order_by_date'] == "order_date_oldest")) {
            $orderOld = array_column($data, 'reviewCreatedOnTime');
            array_multisort($orderOld, SORT_DESC, $data);

            foreach ($data as $row) {
              echo '<tr>
                    <td>' . $row["id"] . '</td>
                    <td>' . $row["reviewFullText"] . '</td>
                    <td>' . date('d M Y H:i:s', $row["reviewCreatedOnTime"]) . '</td>
                    <td>' . $row["reviewerName"] . '</td>
                    <td>' . $row["rating"] . '</td>
                    </tr>';
            }
          }
          ?>
        </tbody>
      </table>

    </div>
  </div>

</body>

</html>